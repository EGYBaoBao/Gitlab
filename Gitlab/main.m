//
//  main.m
//  Gitlab
//
//  Created by 鄂刚洋 on 2017/6/8.
//  Copyright © 2017年 鄂刚洋. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
